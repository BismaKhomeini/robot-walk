**Robot Simulator**

**User Story 1:**
As a robot, I need to have a defined position, starting at the initial location, and facing one of the cardinal directions: WEST, NORTH, EAST, or SOUTH.

**User Story 2:**
As a robot, I need to be able to move both horizontally and vertically, advancing in the direction I am facing.

**User Story 3:**
As a robot, I need the capability to turn either left or right, allowing me to change my orientation as required.

**User Story 4:**
As a robot, I should remain stationary if I am positioned at the corner of the coordinate grid, either at coordinates (0, y) or (x, 0), where 'x' and 'y' represent any non-zero integer values.

* [ ]
